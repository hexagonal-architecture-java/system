package usecase;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({
        //system objects
        "account.system",
        //fake adapters
        "account.adapter"})
public class Build1 {
    // Build 1: Test Adapter -> System <- Mock Adapters
}
