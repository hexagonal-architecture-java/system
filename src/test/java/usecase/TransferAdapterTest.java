package usecase;

import account.system.domain.model.BusinessException;
import account.system.usecase.port.TransferPort;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.inject.Inject;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Use Case - Transfer Service")
@ContextConfiguration(classes = Build1.class)
@ExtendWith(SpringExtension.class)
public class TransferAdapterTest {

    Integer creditAccount = 15;
    Integer debitAccount = 20;
    Integer nonexistentAccount = 30;
    BigDecimal hundred = new BigDecimal(100);
    BigDecimal fifty = new BigDecimal(50);

    @Inject
    TransferPort port;

    @Test
    @DisplayName("get account with null value")
    void test1() {
        try {
            var account = port.getAccount(null);
            assertNull(account, "Account must be null");
        } catch (BusinessException e) {
            fail("Must be load a null account");
        }
    }

    @Test
    @DisplayName("account research with a nonexistent number")
    void test2() {
        try {
            var account = port.getAccount(nonexistentAccount);
            assertNull(account, "Account must be null");
        } catch (BusinessException e) {
            fail("Must be load a null account");
        }
    }

    @Test
    @DisplayName("account research with an existent number")
    void test3() {
        try {
            var account = port.getAccount(creditAccount);
            assertNotNull(account, "Account must be not null");
            System.out.println(account);
        } catch (BusinessException e) {
            fail("Must be load an existent account");
        }
    }

    @Test
    @DisplayName("credit account must be required")
    void test4() {
        try {
            port.transfer(creditAccount, null, fifty);
            fail("credit account is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "credit account is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("debit account must be required")
    void test5() {
        try {
            port.transfer(null, debitAccount, fifty);
            fail("debit account is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "debit account is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("value must be required")
    void test6() {
        try {
            port.transfer(debitAccount, creditAccount, null);
            fail("value is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "value is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("debit account nonexistent")
    void test7() {
        try {
            port.transfer(nonexistentAccount, creditAccount, fifty);
            fail("debit account not found");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "debit account not found");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("credit account nonexistent")
    void test8() {
        try {
            port.transfer(debitAccount, nonexistentAccount, fifty);
            fail("credit account not found");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "credit account not found");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("should return a error message about same account")
    void test9() {
        try {
            port.transfer(debitAccount, debitAccount, fifty);
            fail("The debit and credit account must be different");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "The debit and credit account must be different");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("should transfer 50 successfully")
    void test10() {
        try {
            port.transfer(debitAccount, creditAccount, fifty);
        } catch (BusinessException e) {
            fail("Should not return a transfer error message - " + e.getMessage());
        }
        try {
            var credit = port.getAccount(creditAccount);
            var debit = port.getAccount(debitAccount);
            assertEquals(credit.getBankBalance(), hundred.add(fifty), "Credit bank balance must be equal");
            assertEquals(debit.getBankBalance(), hundred.subtract(fifty), "Debit bank balance must be equal");
        } catch (BusinessException e) {
            fail("Should not return a bank balance validation error - "  + e.getMessage());
        }
    }
}
