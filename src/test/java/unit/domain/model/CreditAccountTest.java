package unit.domain.model;

import account.system.domain.model.Account;
import account.system.domain.model.BusinessException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;

@DisplayName("Credit Account Rules")
public class CreditAccountTest {

    BigDecimal hundred = new BigDecimal(100);
    Account validAccount;

    @BeforeEach
    void prepare() {
        validAccount = new Account(12, hundred, "Rebbeca");
    }

    @Test
    @DisplayName("Should not be possible to deposit a null value")
    public void test1() {
        try {
            validAccount.deposit(null);
            fail("credit value is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "credit value is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should not be possible to deposit a negative value")
    public void test2() {
        try {
            validAccount.deposit(new BigDecimal(-10));
            fail("credit value is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "credit value is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should not be possible to deposit a zero value")
    public void test3() {
        try {
            validAccount.deposit(new BigDecimal(0));
            fail("credit value is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "credit value is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should be possible to deposit a value above zero successful")
    public void test4() {
        try {
            validAccount.deposit(BigDecimal.ONE);
            var totalBankBalance = hundred.add(BigDecimal.ONE);
            assertEquals(validAccount.getBankBalance(), totalBankBalance, "Bank Balance must be equal");
        } catch (BusinessException e) {
            fail("Must credit value with successful - " + e.getMessage());
        }
    }
}
