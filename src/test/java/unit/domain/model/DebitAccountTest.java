package unit.domain.model;

import account.system.domain.model.Account;
import account.system.domain.model.BusinessException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;

@DisplayName("Debit Account Rules")
public class DebitAccountTest {

    BigDecimal hundred = new BigDecimal(100);

    Account validAccount;

    @BeforeEach
    void prepare() {
        validAccount = new Account(12, hundred, "Rebbeca");
    }

    @Test
    @DisplayName("Should not possible to withdrawal amount with null")
    public void test1() {
        try {
            validAccount.withdrawal(null);
            fail("withdrawal amount is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "withdrawal amount is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should not be possible to withdrawal amount with a negative value")
    public void test2() {
        try {
            validAccount.withdrawal(new BigDecimal(-10));
            fail("withdrawal amount is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "withdrawal amount is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should not be possible to withdrawal amount with zero value")
    public void test3() {
        try {
            validAccount.withdrawal(BigDecimal.ZERO);
            fail("withdrawal amount is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "withdrawal amount is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should not be possible to withdrawal amount above bank balance value")
    public void test4() {
        try {
            validAccount.withdrawal(hundred.add(BigDecimal.ONE));
            fail("withdrawal amount exceeds the bank balance value");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "The bank balance is insufficient");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should be possible to withdrawal amount successful")
    public void test5() {
        try {
            validAccount.withdrawal(hundred);
            assertEquals(validAccount.getBankBalance(), BigDecimal.ZERO,  "Bank balance must be zero");
        } catch (BusinessException e) {
            fail("withdrawal must be successful");
        }
    }

    @Test
    @DisplayName("Should be possible to withdrawal amount successful")
    public void test6() {
        try {
            validAccount.withdrawal(BigDecimal.TEN);
            var bankBalanceFinal = hundred.subtract(BigDecimal.TEN);
            assertEquals(validAccount.getBankBalance(), bankBalanceFinal,  "Bank balance must be rest of value");
        } catch (BusinessException e) {
            fail("withdrawal must be successful");
        }
    }
}
