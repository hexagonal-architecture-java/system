package unit.domain.service;

import account.system.domain.model.Account;
import account.system.domain.model.BusinessException;
import account.system.domain.service.Transfer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.math.BigDecimal;

@DisplayName("Transfer Rule")
public class TransferTest {

    BigDecimal hundred = new BigDecimal(100);
    BigDecimal twenty = new BigDecimal(20);
    Transfer transfer;
    Account debitAccount;
    Account creditAccount;

    @BeforeEach
    void prepare() {
        debitAccount = new Account(1, hundred, "Marcelo");
        creditAccount = new Account(2, hundred, "Joanna");
        transfer = new Transfer();
    }

    @Test
    @DisplayName("Should not be possible to transfer a null value")
    void test1(){
        try {
            transfer.transfer(null, debitAccount, creditAccount);
            fail("Transfer value is required");
        } catch ( BusinessException e) {
            assertEquals(e.getMessage(), "Transfer value is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should not be possible to transfer without debit account")
    void test2(){
        try {
            transfer.transfer(twenty, null, creditAccount);
            fail("Debit account is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "Debit account is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should not be possible to transfer without credit account")
    void test3(){
        try {
            transfer.transfer(twenty, debitAccount, null);
            fail("Credit account is required");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "Credit account is required");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should be transfer a value successful")
    void test4(){
        try {
            transfer.transfer(twenty, debitAccount, creditAccount);
            assertEquals(debitAccount.getBankBalance(), hundred.subtract(twenty), "Bank balance value of debit account must be right");
            assertEquals(creditAccount.getBankBalance(), hundred.add(twenty), "Bank balance value of credit account must be right");
        } catch (BusinessException e) {
            fail("Must be transfer successful");
        }
    }
}
