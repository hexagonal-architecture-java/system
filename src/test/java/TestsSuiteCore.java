import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages({
        // business rules tests
        "test.unit.domain.model",
        // services tests
        "test.unit.domain.service",
        // opening port tests
        "test.usecase"
})
public class TestsSuiteCore {

}
