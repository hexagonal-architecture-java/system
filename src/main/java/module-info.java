/**
 * Responsible to define the visibility rules of the system module.
 * resource used by .jar 
 */

module account.system {

    requires javax.inject;
    requires spring.tx;

    // exposing input port (driver)
    exports account.system.usecase.imp;
    exports account.system.usecase.port;

    // exposing business system
    exports account.system.domain.model;
    exports account.system.domain.service;

    // exposing output adapters (driven)
    exports account.system.port;
    exports account.adapter;

    // open spring reflexion
    opens account.system.usecase.imp;
    opens account.system.usecase.port;
    opens account.system.domain.service;
    opens account.adapter;
}