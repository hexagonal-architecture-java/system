package account.system.domain.model;

import java.math.BigDecimal;

import static account.system.domain.model.Error.required;
import static account.system.domain.model.Error.insufficientBankBalance;
import static java.util.Objects.isNull;

public class Account {
    private Integer number;
    private BigDecimal bankBalance;
    private String accountHolder;

    public Account() {
        number = 0;
        bankBalance = BigDecimal.ZERO;
        accountHolder = "not informed";
    }

    public Account(Integer number, BigDecimal bankBalance, String accountHolder) {
        this.number = number;
        this.bankBalance = bankBalance;
        this.accountHolder = accountHolder;
    }

    public void deposit(BigDecimal credit) {
        if (isNull(credit)) {
            required("credit value");
        }

        if (credit.compareTo(BigDecimal.ZERO) <= 0) {
            required("credit value");
        }

        bankBalance = bankBalance.add(credit);
    }

    public void withdrawal(BigDecimal debit)  {
        if (isNull(debit)) {
            required("withdrawal amount");
        }

        if (debit.compareTo(BigDecimal.ZERO) <= 0) {
            required("withdrawal amount");
        }

        if (debit.compareTo(bankBalance) > 0) {
            insufficientBankBalance();
        }

        bankBalance = bankBalance.subtract(debit);
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getBankBalance() {
        return bankBalance;
    }

    public void setBankBalance(BigDecimal bankBalance) {
        this.bankBalance = bankBalance;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    @Override
    public String toString() {
        return "Account{" +
                "number=" + number +
                ", bankBalance=" + bankBalance +
                ", accountHolder='" + accountHolder + '\'' +
                '}';
    }
}
