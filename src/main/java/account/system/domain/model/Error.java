package account.system.domain.model;

public class Error {

    public static void required(String name) {
        throw new BusinessException(name + " is required");
    }

    public static void notfound(String name) {
        throw new BusinessException(name + " not found");
    }

    public static void insufficientBankBalance() {
        throw new BusinessException("The bank balance is insufficient");
    }

    public static void sameAccount() {
        throw new BusinessException("The debit and credit account must be different");
    }
}
