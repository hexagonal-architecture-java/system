package account.system.domain.service;
import account.system.domain.model.Account;

import javax.inject.Named;
import java.math.BigDecimal;

import static java.util.Objects.isNull;
import static account.system.domain.model.Error.*;

@Named
public class Transfer {
    public void transfer(BigDecimal value, Account debit, Account credit) {
        if(isNull(value)) {
            required("transfer value");
        }

        if(isNull(debit)) {
            required("debit account");
        }

        if(isNull(credit)) {
            required("credit account");
        }

        debit.withdrawal(value);
        credit.deposit(value);
    }
}
