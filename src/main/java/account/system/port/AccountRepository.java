package account.system.port;

import account.system.domain.model.Account;

public interface AccountRepository {
    Account get(Integer number);

    void update(Account account);
}
