package account.system.usecase.port;

import account.system.domain.model.Account;
import account.system.domain.model.BusinessException;

import java.math.BigDecimal;

public interface TransferPort {
    Account getAccount(Integer number) throws BusinessException;

    void transfer(Integer debitAccount, Integer CreditAccount, BigDecimal value) throws BusinessException;
}
