package account.system.usecase.imp;

import account.system.domain.model.Account;
import account.system.domain.service.Transfer;
import account.system.port.AccountRepository;
import account.system.usecase.port.TransferPort;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;

import static account.system.domain.model.Error.*;
import static java.util.Objects.isNull;

@Named
public class TransferPortImp implements TransferPort {
    private final AccountRepository repository;
    private final Transfer transfer;

    @Inject
    public TransferPortImp(AccountRepository repository, Transfer transfer) {
        this.repository = repository;
        this.transfer = transfer;
    }

    @Override
    public Account getAccount(Integer number) {
        return this.repository.get(number);
    }

    @Override
    @Transactional
    public void transfer(Integer debitAccount, Integer creditAccount, BigDecimal value) {
        // 1. params validate
        if (isNull(debitAccount)) {
            required("debit account");
        }

        if (isNull(creditAccount)) {
            required("credit account");
        }

        if (isNull(value)) {
            required("value");
        }
        // 2. account validate
        var debit = this.repository.get(debitAccount);
        var credit = this.repository.get(creditAccount);

        if (isNull(debit)) {
            notfound("debit account");
        }

        if (isNull(credit)) {
            notfound("credit account");
        }

        // 3. equal account validate
        if (debit.getNumber().equals(credit.getNumber())) {
            sameAccount();
        }

        // 4. operation
        this.transfer.transfer(value, debit, credit);
        this.repository.update(debit);
        this.repository.update(credit);
    }
}
