package account.adapter;

import account.system.domain.model.Account;
import account.system.domain.model.BusinessException;
import account.system.port.AccountRepository;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.isNull;

@Named
public class AccountAdapterFakeImp implements AccountRepository {

    private Map<Integer, Account> database = new HashMap<>();

    public AccountAdapterFakeImp() {
        database.put(15, new Account(15, new BigDecimal(100), "Peter Fake"));
        database.put(20, new Account(20, new BigDecimal(100), "Sarah Fake"));
    }

    @Override
    public Account get(Integer number) {
        System.out.println("Database fake -> get account(number)");
        return database.get(number);
    }

    @Override
    public void update(Account account) {
        System.out.println("Database fake -> update(account)");
        var acct = database.get(account.getNumber());

        if (! isNull(acct)) {
            database.put(account.getNumber(), account);
        } else {
            throw new BusinessException("Account not exist" + account.getNumber());
        }
    }
}
